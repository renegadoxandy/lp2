<div class="container mt-5">
    <h2>Relatório</h2><br>
        <table class="table table-striped" >
            <thead class='thead-dark'>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Input</th>
                <th scope="col">Request</th>
                <th scope="col">Date</th>
            </tr>
            </thead>
            <tbody>

            <?php foreach ($lista as $item): ?>
                <tr>
                    <th scope="row"><?=$item['id']?></th>
                    <td><?=$item['action']?></td>
                    <td><?=$item['request']?></td>
                    <td><?=$item['updated_at']?></td>
                </tr>
            <?php endforeach; ?>

            </tbody>
        </table>

    
</div>