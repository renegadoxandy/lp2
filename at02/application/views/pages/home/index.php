<div class="container">
    <div class="row mt-5">
        <div class="col-4">
            <img src="<?= base_url('../../assets/img/alexandre.jpg') ?>" class="rounded mx-auto d-block" style="height:500px;">
        </div>
        <div class="col-8">
            <h2>Alexandre Pereira de Freitas</h2><br>
            <h4>GU166097-7</h4><br>
            <h4>NBA Stats</h4><br><br>
            <div class="list-group">
                <a href="<?= base_url('about') ?>" class="list-group-item list-group-item-action">Sobre a API</a>
                <a href="<?= base_url('playerinfo') ?>" class="list-group-item list-group-item-action">Estatística de Jogador</a>
                <a href="<?= base_url('teaminfo') ?>" class="list-group-item list-group-item-action">Estatística de Time</a>
                <a href="<?= base_url('carrerinfo') ?>" class="list-group-item list-group-item-action">Estatística de Carreira</a>
                <a href="<?= base_url('report') ?>" class="list-group-item list-group-item-action">Relatório</a>
            </div>
        </div>
    </div>

    
</div>