<div class="container">
    <div class="row mt-5">
        <h2>CARRER STATS</h2>
    </div>
    <div class="row mt-5">
        <form class="form form-inline">
            <div class="form-group mx-sm-3 mb-2">
                PLAYER ID: 
                <input type="text" class="form-control" id="input-carrer-id" placeholder="203518">
            </div>
            <button type="submit" id="btn-submit" class="btn btn-primary mb-2">Pesquisar</button>
        </form>
    </div>
    <div class="row mt-3" id="result">

    </div>
</div>

<script>
    var div_result = document.querySelector("#result");
    let btn_submit = document.querySelector("#btn-submit");
    
    btn_submit.addEventListener("click", function(event){
        event.preventDefault();
        let inp_carrer_id = document.querySelector("#input-carrer-id");
        let carrer_id = inp_carrer_id.value;
        jsonp(`https://stats.nba.com/stats/playerprofilev2/?PerMode=Totals&PlayerID=${carrer_id}&LeagueID=00`, function(data) {
        console.log(data.resultSets);
        div_result.innerHTML = `
        <h3>${data.resultSets[1].name}</h3>
            <table class='table table-striped table-responsive'>
                <thead class='thead-dark'>
                    <tr>
                    <th scope='col'>${data.resultSets[1].headers[0]}</th>
                        <th scope='col'>${data.resultSets[1].headers[1]}</th>
                        <th scope='col'>${data.resultSets[1].headers[2]}</th>
                        <th scope='col'>${data.resultSets[1].headers[3]}</th>
                        <th scope='col'>${data.resultSets[1].headers[4]}</th>
                        <th scope='col'>${data.resultSets[1].headers[5]}</th>
                        <th scope='col'>${data.resultSets[1].headers[6]}</th>
                        <th scope='col'>${data.resultSets[1].headers[7]}</th>
                        <th scope='col'>${data.resultSets[1].headers[8]}</th>
                        <th scope='col'>${data.resultSets[1].headers[9]}</th>
                        <th scope='col'>${data.resultSets[1].headers[10]}</th>
                        <th scope='col'>${data.resultSets[1].headers[11]}</th>
                        <th scope='col'>${data.resultSets[1].headers[12]}</th>
                        <th scope='col'>${data.resultSets[1].headers[13]}</th>
                        <th scope='col'>${data.resultSets[1].headers[14]}</th>
                        <th scope='col'>${data.resultSets[1].headers[15]}</th>
                        <th scope='col'>${data.resultSets[1].headers[16]}</th>
                        <th scope='col'>${data.resultSets[1].headers[17]}</th>
                        <th scope='col'>${data.resultSets[1].headers[18]}</th>
                        <th scope='col'>${data.resultSets[1].headers[19]}</th>
                        <th scope='col'>${data.resultSets[1].headers[20]}</th>
                        <th scope='col'>${data.resultSets[1].headers[21]}</th>
                        <th scope='col'>${data.resultSets[1].headers[22]}</th>
                        <th scope='col'>${data.resultSets[1].headers[23]}</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>${data.resultSets[1].rowSet[0][0]}</td>
                        <td>${data.resultSets[1].rowSet[0][1]}</td>
                        <td>${data.resultSets[1].rowSet[0][2]}</td>
                        <td>${data.resultSets[1].rowSet[0][3]}</td>
                        <td>${data.resultSets[1].rowSet[0][4]}</td>
                        <td>${data.resultSets[1].rowSet[0][5]}</td>
                        <td>${data.resultSets[1].rowSet[0][6]}</td>
                        <td>${data.resultSets[1].rowSet[0][7]}</td>
                        <td>${data.resultSets[1].rowSet[0][8]}</td>
                        <td>${data.resultSets[1].rowSet[0][9]}</td>
                        <td>${data.resultSets[1].rowSet[0][10]}</td>
                        <td>${data.resultSets[1].rowSet[0][11]}</td>
                        <td>${data.resultSets[1].rowSet[0][12]}</td>
                        <td>${data.resultSets[1].rowSet[0][13]}</td>
                        <td>${data.resultSets[1].rowSet[0][14]}</td>
                        <td>${data.resultSets[1].rowSet[0][15]}</td>
                        <td>${data.resultSets[1].rowSet[0][16]}</td>
                        <td>${data.resultSets[1].rowSet[0][17]}</td>
                        <td>${data.resultSets[1].rowSet[0][18]}</td>
                        <td>${data.resultSets[1].rowSet[0][19]}</td>
                        <td>${data.resultSets[1].rowSet[0][20]}</td>
                        <td>${data.resultSets[1].rowSet[0][21]}</td>
                        <td>${data.resultSets[1].rowSet[0][22]}</td>
                        <td>${data.resultSets[1].rowSet[0][23]}</td>
                    </tr>
                </tbody>
            </table>
        `

        div_result.innerHTML += `
        <h3>${data.resultSets[3].name}</h3>
        <table class='table table-striped table-responsive'>
                <thead class='thead-dark'>
                    <tr>
                    <th scope='col'>${data.resultSets[3].headers[0]}</th>
                        <th scope='col'>${data.resultSets[3].headers[1]}</th>
                        <th scope='col'>${data.resultSets[3].headers[2]}</th>
                        <th scope='col'>${data.resultSets[3].headers[3]}</th>
                        <th scope='col'>${data.resultSets[3].headers[4]}</th>
                        <th scope='col'>${data.resultSets[3].headers[5]}</th>
                        <th scope='col'>${data.resultSets[3].headers[6]}</th>
                        <th scope='col'>${data.resultSets[3].headers[7]}</th>
                        <th scope='col'>${data.resultSets[3].headers[8]}</th>
                        <th scope='col'>${data.resultSets[3].headers[9]}</th>
                        <th scope='col'>${data.resultSets[3].headers[10]}</th>
                        <th scope='col'>${data.resultSets[3].headers[11]}</th>
                        <th scope='col'>${data.resultSets[3].headers[12]}</th>
                        <th scope='col'>${data.resultSets[3].headers[13]}</th>
                        <th scope='col'>${data.resultSets[3].headers[14]}</th>
                        <th scope='col'>${data.resultSets[3].headers[15]}</th>
                        <th scope='col'>${data.resultSets[3].headers[16]}</th>
                        <th scope='col'>${data.resultSets[3].headers[17]}</th>
                        <th scope='col'>${data.resultSets[3].headers[18]}</th>
                        <th scope='col'>${data.resultSets[3].headers[19]}</th>
                        <th scope='col'>${data.resultSets[3].headers[20]}</th>
                        <th scope='col'>${data.resultSets[3].headers[21]}</th>
                        <th scope='col'>${data.resultSets[3].headers[22]}</th>
                        <th scope='col'>${data.resultSets[3].headers[23]}</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>${data.resultSets[3].rowSet[0][0]}</td>
                        <td>${data.resultSets[3].rowSet[0][1]}</td>
                        <td>${data.resultSets[3].rowSet[0][2]}</td>
                        <td>${data.resultSets[3].rowSet[0][3]}</td>
                        <td>${data.resultSets[3].rowSet[0][4]}</td>
                        <td>${data.resultSets[3].rowSet[0][5]}</td>
                        <td>${data.resultSets[3].rowSet[0][6]}</td>
                        <td>${data.resultSets[3].rowSet[0][7]}</td>
                        <td>${data.resultSets[3].rowSet[0][8]}</td>
                        <td>${data.resultSets[3].rowSet[0][9]}</td>
                        <td>${data.resultSets[3].rowSet[0][10]}</td>
                        <td>${data.resultSets[3].rowSet[0][11]}</td>
                        <td>${data.resultSets[3].rowSet[0][12]}</td>
                        <td>${data.resultSets[3].rowSet[0][13]}</td>
                        <td>${data.resultSets[3].rowSet[0][14]}</td>
                        <td>${data.resultSets[3].rowSet[0][15]}</td>
                        <td>${data.resultSets[3].rowSet[0][16]}</td>
                        <td>${data.resultSets[3].rowSet[0][17]}</td>
                        <td>${data.resultSets[3].rowSet[0][18]}</td>
                        <td>${data.resultSets[3].rowSet[0][19]}</td>
                        <td>${data.resultSets[3].rowSet[0][20]}</td>
                        <td>${data.resultSets[3].rowSet[0][21]}</td>
                        <td>${data.resultSets[3].rowSet[0][22]}</td>
                        <td>${data.resultSets[3].rowSet[0][23]}</td>
                    </tr>
                </tbody>
            </table>
        `
        });
        
        var data = {
            action: carrer_id,
            request: `https://stats.nba.com/stats/playerprofilev2/?PerMode=Totals&PlayerID=${carrer_id}&LeagueID=00`
        };

        ajax(data, function(res){
            console.log('O servidor respondeu com:', res);
        });

    })
</script>
