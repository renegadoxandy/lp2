<div class="container">
    <div class="row mt-5">
        <h2>TEAM INFO</h2>
    </div>
    <div class="row mt-5">
        <form class="form form-inline">
            <div class="form-group mx-sm-3 mb-2">
                TEAM ID: 
                <input type="text" class="form-control" id="input-team-id" placeholder="1610612737">
            </div>
            <button type="submit" id="btn-submit" class="btn btn-primary mb-2">Pesquisar</button>
        </form>
    </div>
    <div class="row mt-3" id="result">

    </div>
</div>

<script>
    var div_result = document.querySelector("#result");
    let btn_submit = document.querySelector("#btn-submit");
    
    btn_submit.addEventListener("click", function(event){
        event.preventDefault();
        let inp_team_id = document.querySelector("#input-team-id");
        let team_id = inp_team_id.value;
        jsonp(`https://stats.nba.com/stats/teaminfocommon/?TeamID=${team_id}&LeagueID=00`, function(data) {
        console.log(data.resultSets);
        div_result.innerHTML = `
        <h3>${data.resultSets[0].name}</h3>
            <table class='table table-striped table-responsive'>
                <thead class='thead-dark'>
                    <tr>
                        <th scope='col'>${data.resultSets[0].headers[3]}</th>
                        <th scope='col'>${data.resultSets[0].headers[2]}</th>
                        <th scope='col'>${data.resultSets[0].headers[1]}</th>
                        <th scope='col'>${data.resultSets[0].headers[4]}</th>
                        <th scope='col'>${data.resultSets[0].headers[5]}</th>
                        <th scope='col'>${data.resultSets[0].headers[6]}</th>
                        <th scope='col'>${data.resultSets[0].headers[7]}</th>
                        <th scope='col'>${data.resultSets[0].headers[11]}</th>
                        <th scope='col'>${data.resultSets[0].headers[12]}</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>${data.resultSets[0].rowSet[0][3]}</td>
                        <td>${data.resultSets[0].rowSet[0][2]}</td>
                        <td>${data.resultSets[0].rowSet[0][1]}</td>
                        <td>${data.resultSets[0].rowSet[0][4]}</td>
                        <td>${data.resultSets[0].rowSet[0][5]}</td>
                        <td>${data.resultSets[0].rowSet[0][6]}</td>
                        <td>${data.resultSets[0].rowSet[0][7]}</td>
                        <td>${data.resultSets[0].rowSet[0][11]}</td>
                        <td>${data.resultSets[0].rowSet[0][12]}</td>
                    </tr>
                </tbody>
            </table>
        `

        div_result.innerHTML += `
        <h3>${data.resultSets[1].name}</h3>
            <table class='table table-striped'>
                <thead class='thead-dark'>
                    <tr>
                        <th scope='col'>${data.resultSets[1].headers[0]}</th>
                        <th scope='col'>${data.resultSets[1].headers[1]}</th>
                        <th scope='col'>${data.resultSets[1].headers[2]}</th>
                        <th scope='col'>${data.resultSets[1].headers[3]}</th>
                        <th scope='col'>${data.resultSets[1].headers[4]}</th>
                        <th scope='col'>${data.resultSets[1].headers[5]}</th>
                        <th scope='col'>${data.resultSets[1].headers[6]}</th>
                        <th scope='col'>${data.resultSets[1].headers[7]}</th>
                        <th scope='col'>${data.resultSets[1].headers[8]}</th>
                        <th scope='col'>${data.resultSets[1].headers[9]}</th>
                        <th scope='col'>${data.resultSets[1].headers[10]}</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>NBA</td>
                        <td>${data.resultSets[1].rowSet[0][1]}</td>
                        <td>${data.resultSets[1].rowSet[0][2]}</td>
                        <td>${data.resultSets[1].rowSet[0][3]}</td>
                        <td>${data.resultSets[1].rowSet[0][4]}</td>
                        <td>${data.resultSets[1].rowSet[0][5]}</td>
                        <td>${data.resultSets[1].rowSet[0][6]}</td>
                        <td>${data.resultSets[1].rowSet[0][7]}</td>
                        <td>${data.resultSets[1].rowSet[0][8]}</td>
                        <td>${data.resultSets[1].rowSet[0][9]}</td>
                        <td>${data.resultSets[1].rowSet[0][10]}</td>
                    </tr>
                </tbody>
            </table>
        `
        });

        var data = {
            action: team_id,
            request: `https://stats.nba.com/stats/teaminfocommon/?TeamID=${team_id}&LeagueID=00`
        };

        ajax(data, function(res){
            console.log('O servidor respondeu com:', res);
        });

    })  
</script>
