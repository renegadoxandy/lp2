<div class="container">
    <div class="row mt-5">
        <h2>NBA Stats</h2>
    </div>
    <div class="row mt-5">
        <img src="<?= base_url('../../assets/img/logo.png') ?>" class="rounded mx-auto d-block" style="height:200px;">
    </div>

    <p>Disponibiliza as atuais e o históricos de estatísticas da NBA, isso inclui: jogadores, carreiras, times, ligas, resultados e etc.</p>
    <p>Essas estatísticas podem ser conferidas em: https://stats.nba.com/</p>
    <br>
    <h4>Links de consulta para a api:</h4>
    <p>Request da api detalhadas: https://github.com/seemethere/nba_py/wiki/stats.nba.com-Endpoint-Documentation</p>
    <p>Exemplos de IDs de jogadores e times: https://github.com/bttmly/nba/blob/master/data/players.json</p>
</div>