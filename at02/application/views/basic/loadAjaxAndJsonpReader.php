<script>
function jsonp(url, callback) {
    var callbackName = 'jsonp_callback_' + Math.round(100000 * Math.random());
    window[callbackName] = function(data) {
        delete window[callbackName];
        document.body.removeChild(script);
        callback(data);
    };

    var script = document.createElement('script');
    script.src = url + (url.indexOf('?') >= 0 ? '&' : '?') + 'callback=' + callbackName;
    document.body.appendChild(script);
}

var ajax = (function() {
    function prepareData(data) {
        if (typeof data == 'string') return data;
        var pairs = Object.keys(data).map(function(key) {
            return [key, data[key]].map(encodeURIComponent).join('=');
        }).join('&');

        return pairs;
    }
    return function(_data, cb) {
        var data = prepareData(_data);
        var request = new XMLHttpRequest();
        request.open('POST', '<?= base_url('/ajaxRequestPost') ?>', true);
        request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        request.onload = function() {
            if (request.status >= 200 && request.status < 400) cb(request.responseText);
            else console.log(1, 'Houve um erro :(');
        };
        request.onerror = function() {
            console.log(2, 'Houve um erro :(');
        };
        request.send(data);
    }
})();
</script>