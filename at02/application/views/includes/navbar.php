<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
  <a class="navbar-brand" href="<?= base_url('#')?>">Alexandre</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('/')?>">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('/about')?>">About</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('/playerinfo')?>">Player Info (req 1)</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('/teaminfo')?>">Team Info (req 2)</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('/carrerinfo')?>">Carrer Stats (req 3)</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('/report')?>">Report</a>
      </li>
    </ul>
  </div>
</nav><br>