<?php

class RelatorioModel extends CI_Model {

    public function getDataForm(){
        $data = array(
            'action' => $this->input->post('action'),
            'request' => $this->input->post('request')
        );
        return $data;
    }
    public function store(){
        $data = $this->getDataForm();
        $this->db->insert('relatorio', $data);
    }

    public function selectAll(){
        return $this->db->get('relatorio')->result_array();
    }
}