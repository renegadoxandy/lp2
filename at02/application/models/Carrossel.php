<?php

class Carrossel extends CI_Model {

    private function do_upload(){
        $config = array(
            'upload_path' => "./assets/img",
            'allowed_types' => "gif|jpg|png|jpeg|pdf",
            'overwrite' => TRUE,
            'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
            'max_height' => "2048",
            'max_width' => "4098"
        );
        $upload = ['','',''];
        $this->load->library('upload', $config);
        if($this->upload->do_upload('userfile'))
            $upload[0] = $this->upload->data();
        if($this->upload->do_upload('userfile2'))
            $upload[1] = $this->upload->data();
        if($this->upload->do_upload('userfile3'))
            $upload[2] = $this->upload->data();
        return $upload;
    }

    public function update(){
        $upload = $this->do_upload();
        if($upload[0])
            $this->db->set('url', $upload[0]['file_name'])->where('id', 1)->update('carrossel');
        if($upload[1])
            $this->db->set('url', $upload[1]['file_name'])->where('id', 2)->update('carrossel');
        if($upload[2])
            $this->db->set('url', $upload[2]['file_name'])->where('id', 3)->update('carrossel');
    }

    public function selectAll(){
        return $this->db->get('carrossel')->result_array();
    }
}