<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alexandre extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('RelatorioModel', 'model');
    }

	public function index(){
        $html = $this->load->view('pages/home/index', null, true);
        $this->show($html);
    }

    public function about(){
        $html = $this->load->view('pages/about/index', null, true);
        $this->show($html);
    }
    
    public function playerinfo(){
        $html = $this->load->view('pages/playerinfo/index', null, true);
        $this->show($html);
    }

    public function teaminfo(){
        $html = $this->load->view('pages/teaminfo/index', null, true);
        $this->show($html);
    }

    public function carrerinfo(){
        $html = $this->load->view('pages/carrerinfo/index', null, true);
        $this->show($html);
    }

    public function report(){
        $data['lista'] = $this->model->selectAll();
        $html = $this->load->view('pages/list/index', $data, true);
        $this->show($html);
    }
 
   public function ajaxRequestPost(){
        $this->model->store();
        echo 'Adicionado com sucesso!';  
   }
}