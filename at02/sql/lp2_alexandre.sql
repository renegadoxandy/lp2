-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 12-Maio-2019 às 09:20
-- Versão do servidor: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lp2_alexandre`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `relatorio`
--

CREATE TABLE `relatorio` (
  `id` int(11) NOT NULL,
  `action` varchar(100) NOT NULL,
  `request` varchar(100) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `relatorio`
--

INSERT INTO `relatorio` (`id`, `action`, `request`, `updated_at`, `deleted`) VALUES
(2, '203518', 'https://stats.nba.com/stats/commonplayerinfo/?PlayerID=203518', '2019-05-09 21:52:52', 0),
(3, '203518', 'https://stats.nba.com/stats/commonplayerinfo/?PlayerID=203518', '2019-05-09 22:01:36', 0),
(4, '203518', 'https://stats.nba.com/stats/commonplayerinfo/?PlayerID=203518', '2019-05-09 22:04:33', 0),
(5, '1610612737', 'https://stats.nba.com/stats/teaminfocommon/?TeamID=1610612737&LeagueID=00', '2019-05-09 22:06:59', 0),
(6, '1610612737', 'https://stats.nba.com/stats/teaminfocommon/?TeamID=1610612737&LeagueID=00', '2019-05-09 22:27:03', 0),
(7, '1610612738', 'https://stats.nba.com/stats/teaminfocommon/?TeamID=1610612738&LeagueID=00', '2019-05-09 22:27:11', 0),
(8, '203518', 'https://stats.nba.com/stats/commonplayerinfo/?PlayerID=203518', '2019-05-09 22:33:42', 0),
(9, '', 'https://stats.nba.com/stats/playerprofilev2/?PerMode=Totals&PlayerID=&LeagueID=00', '2019-05-10 00:02:20', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `relatorio`
--
ALTER TABLE `relatorio`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `relatorio`
--
ALTER TABLE `relatorio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
